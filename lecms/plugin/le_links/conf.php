<?php
return array(
	'name' => '友情链接',	// 插件名
	'brief' => '友情链接插件，简单的文字链接。',
	'version' => '1.0.0',			// 插件版本
	'cms_version' => '3.0.0',		// 插件支持的程序版本
	'update' => '2022-12-16',		// 插件最近更新
	'author' => '大大的周',				// 插件作者
	'authorurl' => 'https://www.lecms.cc',	// 插件作者主页
	'setting' => '',		// 插件设置URL
);
