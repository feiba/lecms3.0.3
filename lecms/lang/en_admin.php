<?php
/**
 * Author: dadadezhou <zhoudada97@foxmail.com>
 * Date: 2022-12-14
 * Time: 12:22
 * Description:英文语言包 后台（注意， 不能在数组值的后面加 //注释 这种格式 ， 只能用 /星号 注释 星号/ 这种）
 */
return array(
    'admin_login'=>'Administrator Login',
    'login1'=>'Login',
    'home'=>'Home',
    'admin_manage'=>'Admin manage',
    'disable_1'=>'Disable',
    'username'=>'User Name',
    'email'=>'Email',
    'password'=>'Password',
    'sending'=>'Sending',
    'forgot_pw'=>'Forgot password',
    'batch_add'=>'Batch add',
    'view'=>'View',
    'author'=>'Author',
    'mobile'=>'Mobile',
    'edit_information'=>'Edit information',
    'edit_password'=>'Edit password',
    'batch_delete'=>'Batch delete',
    'edit_pwd'=>'Edit pwd',
    'edit_pwd1'=>'Edit password',
    'new_password'=>'New password',
    'old_password'=>'Old password',
    'confirm_new_password'=>'New password',
    'input_old_password'=>'Old password',
    'input_new_password'=>'New password with 6~32 characters',
    'input_confirm_new_password'=>'Enter new password again',
    'register'=>'Register',
    'setting'=>'Setting',
    'clear'=>'Clear',
    'refresh'=>'Refresh',
    'full_screen'=>'Full screen',
    'close_website'=>'Close website',
    'close_search'=>'Close Search',
    'close_views'=>'Close Views',
    'close_views_tips'=>'Turn off content view updates, recommended for high concurrency sites to enable',
    'set_flags'=>'Flags',
    'flags'=>'Flags',
    'flag_manage'=>'Flag Content',
    'auto_pic'=>'Auto Pic',
    'auto_pic_tips'=>'Automatically extract images from the content as pic when no pic are uploaded',

    'close_current'=>'Close current',
    'close_other'=>'Close other',
    'close_all'=>'Close all',
    'change_color'=>'Change Color',
    'form_invalid'=>'The form is invalid. Please refresh and try again',
    'password_error'=>'Password error',
    'refresh_success'=>'Refresh success',
    'select_module'=>'Select Module',
    'website'=>'Index',
    'statistics'=>'Statistics',
    'category_total'=>'Category Count',
    'comment_total'=>'Comment Count',
    'user_total'=>'User Count',
    'cms_info'=>'CMS info',
    'cms_version'=>'CMS version',
    'version'=>'Version',
    'developer'=>'Developer',
    'developer_author'=>'dadadezhou',
    'bbs'=>'BBS',
    'thanks_much'=>'Tanks',
    'my_info'=>'My info',
    'login_username'=>'Username',
    'contents_count'=>'Contents',
    'logins_count'=>'Logins',
    'this_login'=>'This Login',
    'last_login'=>'Last Login',
    'last_login_time'=>'Last Login Time',
    'reg_time'=>'Registration',
    'browser'=>'Browser',
    'being_used'=>'In use',
    'install_successfully'=>'Install successfully',
    'enable_successfully'=>'Enable successfully',
    'disable_successfully'=>'Disable successfully',
    'search'=>'Search',
    'select_data'=>'Please select data',
    'num_del_failed'=>' content deletion failed',
    'homepage'=>'Homepage',
    'intro'=>'Intro',
    'thumb'=>'Picture',
    'upload_pic'=>'Upload',
    'copy'=>'Copy',
    'copy_successfully'=>'Copy successfully',
    'copy_failed'=>'Copy failed',

    'server_info'=>'Server',
    'os_info'=>'OS',
    'software_info'=>'Software',
    'php_info'=>'PHP',
    'mysql_info'=>'MySQL',
    'filesize_info'=>'Filesize',
    'exectime_info'=>'Exectime',
    'safe_mode_info'=>'Safe Mode',
    'url_fopen_info'=>'Remote Access',
    'space_info'=>'Space',
    'other_info'=>'Other',
    'second'=>' Second',
    'default'=>'Default',
    'test'=>'Test',

    //后台菜单
    'manage'=>'',
    'category_manage'=>'Category',
    'page_manage'=>'Page',
    'content_manage'=>'Content',
    'user_manage'=>'User',
    'plugin_theme'=>'Plugin',
    'tool_manage'=>'Tools',
    'basic_setting'=>'Basic Setting',
    'seo_setting'=>'SEO Setting',
    'link_setting'=>'Links Setting',
    'sitemap_setting'=>'Sitemap Setting',
    'user_setting'=>'User Setting',
    'attach_setting'=>'Attach Setting',
    'image_setting'=>'Image Setting',
    'comment_setting'=>'Comment Setting',
    'email_setting'=>'Email Setting',
    'security_setting'=>'Security Setting',
    'other_setting'=>'Other Setting',
    'cate_manage'=>'Category',
    'navigate_manage'=>'Navigate',
    'mobil_navigate_manage'=>'Mobile Navigate',
    'nav_location'=>'Navigate Location',
    'nav_location_tips'=>'Webpage Needs to be Refreshed',
    'add_nav_location'=>'Add Navigate Location',
    'has_nav_location'=>'Navigate Location',
    'nav_location_alias'=>'Navigate Alias',
    'nav_location_name'=>'Navigate Name',
    'nav_location_alias_no_empty'=>'Navigate Alias Not Empty',
    'nav_location_alias_error'=>'Navigate Alias Can only be numbers and lowercase letters',
    'nav_location_alias_exists'=>'Navigate Alias Already Exists',
    'nav_location_alias_tips'=>'Numbers and lowercase letters, non repeatable',
    'nav_location_name_no_empty'=>'Navigate Name Not Empty',
    'nav_location_name_tips'=>'Admin Menu Display',
    'article_manage'=>'Article',
    'tags_manage'=>'Tags',
    'comment_manage'=>'Comments',
    'attach_manage'=>'Attachs',
    'model_manage'=>'Models',
    'user_manage'=>'Users',
    'user_group_manage'=>'User Group',
    'plugin_manage'=>'Plugins',
    'theme_manage'=>'Themes',
    'clear_cache'=>'Clear Cache',
    'clear_log'=>'Clear Log',
    'rebuild_statistics'=>'Rebuild',
    'debug_mod'=>'Debug',
    'debug_mod_admin'=>'Admin Debug',
    'lang_mod'=>'Language',
    'lang_mod_admin'=>'Admin Lang.',
    'refresh_page'=>'Refresh page after changes',
    'db_dictionary'=>'DB dict',
    'db_tips'=>'If your database account does not have advanced operating privileges, you will not be able to get the data table information',
    'optimize_table'=>'Optimize',
    'repair_table'=>'Repair',
    'check_table'=>'Check',
    'table_structure'=>'Structure',
    'db_table_name'=>'Table Name',
    'db_table_comment'=>'Table Comment',
    'db_table_rows'=>'Rows',
    'db_table_data_size'=>'Data Size',
    'db_table_data_free'=>'Data Free',
    'db_table_engine'=>'Engine',
    'db_table_collation'=>'Collation',
    'db_table_update_time'=>'Update Time',

    'webname'=>'Web Name',
    'webdomain'=>'Domain',
    'webdir'=>'Install Dir',
    'beian'=>'ICP',
    'webmail'=>'Email',
    'webqq'=>'QQ',
    'webweixin'=>'WeChat',
    'webtel'=>'Tel',
    'tongji'=>'Statistics',
    'copyright'=>'Copyright',
    'link_tag_type'=>'Tag Link',
    'tag_name'=>'Tag Name',
    'tag_id'=>'Tag ID',
    'mobile_view'=>'Mobile',
    'mobile_view_dir'=>'Mobile Theme',
    'admin_layout'=>'Layout',
    'left_menu'=>'Left Menu',
    'left_and_top_menu'=>'Left and Top Menu',

    'please_input_username'=>'Please input username',
    'please_input_password'=>'Please input password',
    'please_input_vcode'=>'Verification code',
    'login_successfully'=>'Login successfully',
    'logout_successfully'=>'Logout successfully',
    'please_try_15_min'=>'Please try again in 15 minutes',
    'username_password_error'=>'Username or password error',
    'access_dis1'=>'No access',
    'access_dis2'=>'No access',
    'access_dis2_log'=>'Unauthorized user group attempts to log in to the Admin',
    'please_login_again'=>'Please login again',
    'illegal_access'=>'Illegal access',

    //user_model
    'username_dis_empty'=>'Username cannot be empty',
    'username_dis_over_16'=>'Username cannot be greater than 16 characters',
    'username_has_illegal_characters'=>'Username contains illegal characters',
    'username_has_illegal_bracket'=>'Username contains <>',
    'password_dis_empty'=>'Password cannot be empty',
    'password_dis_less_6'=>'Password cannot be less than 6 characters',
    'password_dis_over_32'=>'Password cannot be greater than 32 characters',
    'user_not_exist'=>'User does not exist',
    'initial_administrator_dis_delete'=>'The initial administrator cannot be deleted',

    'common_manage'=>'Common',
    'php_version_5_5'=>'PHP version is required to be greater than 5.5',

    'email_send_successfully'=>'Mail sending successfully',
    'email_send_failed'=>'Mail sending failed',
    'open_email'=>'Open mail',
    'email_smtp'=>'SMTP Server',
    'email_port'=>'SMTP Port',
    'email_account'=>'Send mail',
    'email_account_name'=>'Send Name',
    'receive_email_not_empty'=>'The receiving mail cannot be empty',
    'open_email_0'=>'Mail disabled',
    'email_smtp_not_empty'=>'SMTP Server is empty',
    'email_port_not_empty'=>'SMTP Port is empty',
    'email_account_not_empty'=>'Send mail is empty',
    'send_mail_password_not_empty'=>'Send mail password is empty',
    'email_test_title'=>'test email title',
    'email_test_body'=>'test email body',

    'home_seo_setting'=>'Home SEO Setting',
    'show_seo_setting'=>'Content SEO Rule Setting',
    'seo'=>'SEO',
    'seo_title'=>'SEO Title',
    'seo_keywords'=>'Keywords',
    'seo_description'=>'Description',
    'variable'=>'Variable',
    'variable1'=>'{webname}：Site Name',
    'variable2'=>'{title}：Content Title，{seo_title}：Content SEO Title，{seo_keywords}：Content SEO Keywords，{seo_description}：Content SEO Description，{intro}：Content Intro，{tags}：Content TAG，{page}：Content Pages',
    'variable3'=>'{cate_name}：Category Name，{cate_seo_title}：Category SEO Title，{cate_seo_keywords}：Category SEO Keywords',

    'url_rewrite'=>'URL Rewrite',
    'url_option'=>'URL Option',
    'url_option_0'=>'URL Dynamic',
    'url_option_1'=>'URL Rewrite',
    'your_server'=>'Your Server：',
    'nginx_info'=>'Please modify nginx.conf，Add the above rules between location/{ } nodes',
    'htaccess_is_exists'=>'.htaccess file already exists，If you do not use URL Rewrite, please click the "Delete .htaccess" button to delete the .htaccess file',
    'htaccess_no_exists'=>'.htaccess file does not exists，You can click the "Create .htaccess" button or manually create a .htaccess file in the root directory of the website and copy the above contents',
    'webconfig_is_exists'=>'web.config file already exists，If you do not use URL Rewrite, please click the "Delete web.config" button to delete the web.config file',
    'webconfig_no_exists'=>'web.config file does not exists，You can click the "Create web.config" button or manually create a web.config file in the root directory of the website and copy the above contents',
    'httpdini_is_exists'=>'httpd.ini file already exists，If you do not use URL Rewrite, please click the "Delete httpd.ini" button to delete the httpd.ini file',
    'httpdini_no_exists'=>'httpd.ini file does not exists，You can click the "Create httpd.ini" button or manually create a httpd.ini file in the root directory of the website and copy the above contents',
    'no_write_permission'=>'No write permission',
    'config_inc_not_write'=>'File lecms/config/config.inc.php no write permission',
    'config_inc_write_failed'=>'Write in config.inc.php failed',

    'link_cate_page_pre_not_empty'=>'category link prefix cannot be empty',
    'link_cate_page_end_not_empty'=>'category link suffix cannot be empty',
    'link_cate_end_not_empty'=>'category link first page suffix cannot be empty',
    'link_tag_pre_not_empty'=>'tag link prefix cannot be empty',
    'link_tag_end_not_empty'=>'tag link suffix cannot be empty',
    'link_tag_top_not_empty'=>'Hot tag link cannot be empty',
    'link_comment_pre_not_empty'=>'comment link prefix cannot be empty',
    'link_space_pre_not_empty'=>'Personal homepage link prefix cannot be empty',
    'link_space_end_not_empty'=>'Personal homepage link suffix cannot be empty',
    'content_url'=>'Content URL',
    'content_url_option_0'=>'Number',
    'content_url_option_1'=>'Recommend',
    'content_url_option_2'=>'Alias',
    'content_url_option_3'=>'Password',
    'content_url_option_4'=>'ID',
    'content_url_option_5'=>'Alias combination',
    'content_url_option_6'=>'Custom',
    'content_url_option_6_tips'=>'Use carefully',
    'category_url'=>'Category URL',
    'category_homepage_url'=>'Index URL',
    'tag_url'=>'Tag URL',
    'tag_top'=>'Hot Tag',
    'comment_url'=>'Comment URL',
    'space_url'=>'Personal URL',
    'prefix'=>'Prefix',
    'suffix'=>'Suffix',
    'duplicate_prefix'=>'There are duplicate prefix settings',

    'open_user'=>'User module',
    'open_user_register'=>'Register',
    'open_user_register_vcode'=>'Register code',
    'open_user_login'=>'Login',
    'open_user_login_vcode'=>'Login code',
    'open_user_reset_password'=>'Find password',
    'open_email_enabled'=>'Email function needs to be enabled',
    'user_avatar_width'=>'Avatar width',
    'user_avatar_height'=>'Avatar height',
    'suggest'=>'Suggest',

    'admin_security_setting'=>'Admin Security Settings',
    'admin_vcode'=>'Admin code',
    'admin_safe_entrance'=>'Admin Safe URL',
    'admin_safe_entrance_tips'=>'Open the backend security entrance, please remember the backend login URL',
    'admin_safe_auth'=>'Security key',
    'admin_safe_auth_reset'=>'Reset Key',
    'admin_safe_url'=>'Login URL',
    'del_install_tips'=>'Suggest deleting the install folder',
    'default_admin_dir_tips'=>'Suggest modifying the name of the admin folder',

    'image'=>'Image',
    'attach'=>'Attach',
    'allow_ext'=>'Allow Type',
    'max_size'=>'Max Size',
    'open_comment'=>'Comment',
    'comment_vcode'=>'Comment code',
    'no_login_comment'=>'Tourist comments',
    'default_author'=>'Default Author',
    'thumb_setting'=>'Thumbnail Setting',
    'thumb_type'=>'Cropping',
    'thumb_type_1'=>'Filler',
    'thumb_type_2'=>'Center',
    'thumb_type_3'=>'Upper Left',
    'thumb_quality'=>'Thumbnail quality',
    'watermark_setting'=>'Watermark Setting',
    'watermark_pos'=>'Watermark position',
    'watermark_pct'=>'Watermark transparency',
    'watermark_file'=>'Watermark File Path',
    'watermark_info'=>'Replace files to achieve your own watermark effect',
    'rand'=>'Rand',
    'db_cache'=>'Database Cache',
    'file_cache'=>'File Cache',
    'log_error'=>'Error Log',
    'log404'=>'404 Log',
    'log_login'=>'Admin Login Log',
    're_cate'=>'Category contents count statistics',
    're_table'=>'Database statistics',
    're_user_content'=>'User contents count statistics',
    'clear_success'=>'Clear successfully',
    'rebuild_success'=>'Rebuild successfully',
    'file_not_exist'=>'File does not exist',

    'basic_info'=>'Basic info',
    'extend_info'=>'Extend info',
    'contribute'=>'Contribute',
    'page_content'=>'Page Content',
    'cid'=>'cid',
    'cate_name'=>'Category Name',
    'category_name'=>'Name',
    'model'=>'Model',
    'type'=>'Type',
    'cate_type_1'=>'Channel',
    'cate_type_0'=>'List',
    'alias'=>'Alias',
    'cate_count'=>'Content Count',
    'channel_tpl'=>'Channel Template',
    'cate_tpl'=>'List Template',
    'tpl_info'=>'Template info',
    'show_tpl'=>'Content Template',
    'page_tpl'=>'Page Template',
    'orderby'=>'Sort',
    'opt'=>'Operation',
    'add_cate'=>'Add Category',
    'edit_cate'=>'Edit Category',
    'add_page_cate'=>'Add Page',
    'edit_page_cate'=>'Edit Page',
    'select_category'=>'Select Category',
    'category'=>'Category',
    'links'=>'Link',
    'no_category_data'=>'No category，please add category',
    'target'=>'Target',
    'add_navigate'=>'Add navigate',
    'link_name'=>'Name',
    'input_link_name'=>'Input link name',
    'input_link_url'=>'Input link URL',
    'input_link_css_class'=>'Input CSS class',
    'del_son_navigate_tips'=>' and son navigate',
    'select_upid'=>'Parent',
    'cms_page_upid'=>'Parent',
    'select_cate_model'=>'Please select model',
    'select_cate_type'=>'Please select type',
    'select_cate_upid'=>'Please select Parent',
    'input_cate_name'=>'Category Name cannot be empty',
    'input_cate_alias'=>'Alias cannot be empty',
    'cate_alias_than_50'=>'Alias cannot exceed 50 characters',
    'cate_tpl_no_empty'=>'List Template cannot be empty',
    'show_tpl_no_empty'=>'Content Template cannot be empty',
    'upid_not_my'=>'Parent category cannot be its own',
    'dis_category_edit_model'=>'Category has lower level, Model cannot be modified',
    'dis_category_edit_type'=>'Category has lower level, Type cannot be modified',
    'dis_category_edit_model_1'=>'Category has contents, Model cannot be modified',
    'dis_category_edit_type_1'=>'Category has contents, Type cannot be modified',
    'dis_category_delete_level'=>'Category has lower level, cannot be delete',
    'dis_category_delete_contents'=>'Category has lower level, cannot be delete',
    'category_no_empty'=>'Category cannot be empty',
    'change_category'=>'Change Category',

    'model_field'=>'Field',
    'mid'=>'mid',
    'model_name'=>'Model Name',
    'table'=>'Table',
    'pic_width'=>'Thumb Width',
    'pic_height'=>'Thumb Height',
    'menu_icon'=>'Menu icon',
    'add_model'=>'Add Model',
    'system_model_no_delete'=>'System built-in model cannot be deleted',
    'page_model_no_field'=>'Page model no fields',
    'no_field_plugin'=>'You need to install and enable the Custom Field Plugin',
    'modelname_no_empty'=>'Model Name cannot be empty',
    'modeltablename_no_empty'=>'Model table cannot be empty',
    'modeltablename_is_exist'=>'Model table already exists',
    'modeltablename_no_safe'=>'Model table can only be lowercase English letters',
    'page_model_edit_tpl'=>'Page model can only modified List Template',
    'width_and_height_no_0'=>'Thumb Width and height cannot be empty',
    'all_no_empty'=>'All items cannot be empty',
    'tpl_tips'=>'Channel Template、List Template、Content Template cannot be the same',

    'no_install_plugin'=>'Plug in not installed',
    'no_enabled_plugin'=>'Plug in not enabled',
    'enabled_plugin'=>'Plug enabled',
    'plugin_dir_no_empty'=>'Plug dir is empty',
    'plugin_dir_no_safe'=>'Plug dir is composed of numbers and letters',
    'plugin_dir_no_exists'=>'Plug dir no exists',
    'plugin_is_installed'=>'Plug in already installed',
    'plugin_version_failed'=>'Plug in installation failed, CMS system version',
    'theme_dir_no_empty'=>'Theme dir is empty',
    'theme_dir_no_safe'=>'Theme dir is composed of numbers and letters',
    'theme_dir_no_exists'=>'Theme dir no exists',
    'write_config_failed'=>'Write plugin.inc.php failed',

    'user_group'=>'User Group',
    'all_user_group'=>'All User Group',
    'select_user_group'=>'Select User Group',
    'uid'=>'UID',
    'add_user'=>'Add User',
    'edit_user'=>'Edit User',
    'new_pwd_no_empty'=>'New password cannot be empty',
    'system'=>'System',
    'group_id'=>'Group ID',
    'add_group'=>'Add Group',
    'please_select_group'=>'Please select a user group',
    'groupname_no_empty'=>'User group name cannot be empty',
    'system_group_no_delete'=>'System built-in user group name cannot be deleted',
    'admin_group_dis_edit'=>'Administrators user group cannot be modified',
    'admin1_group_dis_edit'=>'Administrators user group cannot be modified',
    'groupname_is_exists'=>'User group name already exists',
    'email_is_exists'=>'Email already exists',
    'username_is_exists'=>'Username already exists',
    'old_pwd_no_empty'=>'Old password cannot be empty',
    'old_pwd_error'=>'Old password error',
    'new_pwd_inconsistent'=>'New passwords entered twice are inconsistent',
    'uid_1_dis_delete'=>'Initial administrator cannot be deleted',
    'prohibit_delete_self'=>'Cannot delete oneself',
    'groupname_exists_user'=>'User group has users, cannot be deleted',

    'cmsid'=>'ID',
    'id'=>'ID',
    'filename'=>'Filename',
    'ext'=>'Ext',
    'filesize'=>'Filesize',
    'filepath'=>'Filepath',
    'uploadtime'=>'Upload time',
    'downloads'=>'Downloads',
    'golds'=>'Golds',
    'credits'=>'Credits',
    'del_invalid_attach'=>'Delete invalid files',
    'del_invalid_attach_tips'=>'Delete 100 invalid files each time. Are you sure to delete?',
    'write_attach_failed'=>'Failed to write attachment table',
    'write_table_failed'=>'Failed to write database table',

    'comment'=>'Comment',
    'title'=>'Title',
    'date'=>'Date',
    'ip'=>'IP',
    'content'=>'Content',
    'tagname'=>'Tag Name',
    'tag'=>'Tag',
    'add_tag'=>'Add Tag',
    'edit_tag'=>'Edit Tag',
    'one_line_tag'=>'One tag name per line',
    'tag_exists'=>'Tag name already exists',

    'export_url'=>'Export URL',
    'source'=>'Source',
    'views'=>'Views',
    'flag'=>'Flag',
    'jump_url'=>'Jump URL',
    'tag_tips'=>'Multiple tag are separated by English commas',
    'alias_tips'=>'number letter - _',
    'isremote'=>'Remote picture localization',
    'title_no_empty'=>'Please fill in the title',

    'flag_1'=>'Recommend',
    'flag_2'=>'Hot',
    'flag_3'=>'Headline',
    'flag_4'=>'Choice',
    'flag_5'=>'Slide',

    'f_links'=>'Links',
    'links_name'=>'Link Name',
    'links_name_no_empty'=>'Link Name cannot be empty',
    'links_url_no_empty'=>'Link URL cannot be empty',
    'add_f_links'=>'Add Link',
    'edit_f_links'=>'Edit Link',

    'php_error'=>'Error Log',
    'php_error404'=>'404 Log',

    'one_line_one'=>'One in a row',
    'opt_confirm'=>'Confirm ?',

    'debug_tips'=>'Your Lecms has debug mode turned on, this mode of the site is at risk of attack and performance degradation. Please turn off the debug mode in time for the actual deployment of the site online!',
    'url_rewrite_tips'=>'Enable URL pseudo-static, more friendly to search engines and users and better security',
    'plugin_disable'=>'All plugins have been disabled, Please modify the lecms/config/config.inc.php file plugin_disable=>0',
    'characters_too_long'=>'{field} cannot exceed {length} characters',
    'url_path'=>'URL',
    'url_relative_path'=>'Relative URL',
    'url_absolute_path'=>'Absolute URL',

    // hook lang_en_admin.php
);