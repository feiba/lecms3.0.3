<?php
/**
 * Author: dadadezhou <zhoudada97@foxmail.com>
 * Date: 2022-12-20
 * Time: 11:52
 * Description: LECMS 安装语言包
 */
return array(
    'installation_wizard'=>'LECMS installation wizard',
    'license_title'=>'License Agreement',
    'license_content' => 'Thank you for choosing LECMS. LECMS has the characteristics of safety, efficiency, stability, fast speed and super load, especially in the case of large data volume, its excellence is even more outstanding.
<br><br>
LECMS can load the templates on the mobile end and PC separately, and the URL remains unchanged. It has a very convenient plug-in and template development mechanism.
<br><br>
LECMS has only 20 tables and runs very fast. It processes a single request in 0.01 seconds. The separation of front and rear code and support of user-defined model content is a very good cornerstone of secondary development.
<br><br>
Layuimini is used as the back-end class library to fully support mobile browsers; The back-end is based on PHP 5.3+MySQL and supports NoSQL operations such as Apc/Yac/Redis/Memcached.
<br><br>
LECMS is development based on XiunoPHP open source framework, released under the MIT agreement, and you can freely modify and derive versions for commercial use without worrying about any legal risks (the original copyright information should be retained after modification).
<br><br>
The LECMS main program is free of charge and does not provide any form of free service. The user shall not apply the system to any form of illegal use, and all legal risks arising therefrom shall be borne by the user, which has nothing to do with the website and the developer. Once you download, install and use LECMS, you acknowledge that you have read, understood and agreed to be bound by this clause and comply with all relevant laws and regulations. If you do not agree to such terms, please do not use this program.
<br><br>
Warning: According to Chinese laws, do not disseminate relevant resources in any form (including but not limited to resource data files, seed files, online disk files, FTP files, etc.) without obtaining the authorization of relevant resources (including but not limited to films, animations, books, music, etc.).',
    'agree_license_to_continue' => 'Agree to continue to install the agreement',
    'no_agree'=>'Disagree',
    'step_1_title' => 'Environmental Check',
    'runtime_env_check' => 'Runtime environment detection',
    'required' => 'Required',
    'current' => 'Current',
    'dir'=>'Dir',
    'os' => 'OS',
    'php_version' => 'PHP Version',
    'file_uploads'=>'Upload limit',
    'disk_free_space'=>'Disk space',
    'mysql'=>'Mysql',
    'gd'=>'GD extend',
    'open'=>'Open',
    'close'=>'Close',
    'writable' => 'Writable',
    'unwritable' => 'Unwritable',
    'unix_like'=>'UNIX Like',
    'next_step'=>'Next',
    'check_again'=>'Check again',
    'close_tips_1'=>'System cannot be used if it is closed',
    'close_tips_2'=>'Not support thumbnails, watermarks and verification codes when closed',
    'close_tips_3'=>'Templates and plugin cannot be installed online when closed',

    'db_and_admin'=>'Database info and administrators',
    'step_2_title' => 'Database',
    'administrators'=>'Administrators',
    'db_host' => 'DB Host',
    'db_host_tip'=>'Default is localhost or 127.0.0.1',
    'db_port' => 'DB Post',
    'db_port_tip' => 'Default is 3306',
    'db_name' => 'DB Name',
    'db_user' => 'DB User',
    'db_pass' => 'Password',
    'db_user_tip' => 'Database username',
    'db_pass_tip' => 'Database password',
    'db_prefix'=>'Table Prefix',
    'username'=>'Username',
    'password'=>'Password',
    'author'=>'Author',
    'author_name'=>'administrators',
    'password_tips'=>'6~32 characters',
    'cover'=>'Overlay Install',
    'yes'=>'Yes',
    'install'=>'Install',
    'no_mysql_extend'=>'No database extension, unable to install',
    'no_mysql'=>'No mysql、mysqli、pdo_mysql extension',
    'installation_info'=>'Installation Information',
    'installed_tips' => 'You have been installed, and if you need to re install, please delete the lecms/config/config.inc.php!',

    'data_error'=>'Data error',
    'db_host_no_empty'=>'Database host cannot be empty',
    'db_port_no_empty'=>'Database port cannot be empty',
    'db_name_no_empty'=>'Database Name cannot be empty',
    'db_name_error'=>'Database name can only be letter numbers _',
    'db_prefix_no_empty'=>'Database prefix cannot be empty',
    'db_prefix_error'=>'Database prefix can only be lower case letter _',
    'username_no_empty'=>'Username host cannot be empty',
    'username_dis_less_2'=>'Username cannot be less than 2 characters',
    'username_dis_over_16'=>'Username cannot be greater than 16 characters',
    'password_dis_less_6'=>'Password cannot be less than 6 characters',
    'password_dis_over_32'=>'Password cannot be greater than 32 characters',

    'clear'=>'Clear',
    'setting'=>'Setting',
    'successfully'=>'successfully',
    'failed'=>'failed',
    'plugin_file_non_existent'=>'plugin.sample.php file non-existent',
    'config_file_non_existent'=>'config.sample.php file non-existent',
    'install_successfully'=>'Congratulations，Your website has been installed',
    'home_url'=>'Home URL',
    'admin_url'=>'Admin URL',
    'delete_install_dir'=>'For more security, please delete the install folder',
);